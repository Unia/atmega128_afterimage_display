/*
* atmega128_afterimage_display.c
* afterimage display
* interrupt switch1 - pointer increase, interrupt switch1 - pointer descent
* pointer data show on afterimage screen
* Created: 2017-09-29 pm 10:10:21
* Author : kang je kwang
*/

#define F_CPU 16000000L

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "afterimage_display.h"

int led_data_point_num = 0;

ISR(INT0_vect){
	led_data_point_num--;
	if(led_data_point_num < 0){
		led_data_point_num = 0;
	}
	_delay_ms(100);
}

ISR(INT1_vect){
	led_data_point_num++;
	if(led_data_point_num > 3){
		led_data_point_num = 3;
	}
	_delay_ms(100);
}

int main(void){
	int data_size = 15;
	
	const short LED_T [] = {0x0000, 0x0000, 0xF800, 0xF800, 0xF800, 0xF800, 0xF800, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xF800, 0xF800, 0xF800, 0xF800, 0xF800, 0x0000, 0x0000, 0x0000};
	const short LED_H [] = {0x0000, 0x0000, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0x03C0, 0x03C0, 0x03C0, 0x03C0, 0x03C0, 0x03C0, 0x03C0, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0x0000, 0x0000};
	const short LED_A [] = {0x0000, 0x0000, 0x03FF, 0x0FFF, 0x1FFF, 0x3CE0, 0x70E0, 0xE0E0, 0xE0E0, 0xC0E0, 0xE0E0, 0xE0E0, 0x70E0, 0x3CE0, 0x1FFF, 0x0FFF, 0x03FF, 0x0000, 0x0000};
	const short LED_O [] = {0x0000, 0x0000, 0x0FF0, 0x3FFC, 0x7FFE, 0xF00F, 0xE007, 0xE007, 0xE007, 0xC003, 0xE007, 0xE007, 0xE007, 0xF00F, 0x7FFE, 0x3FFC, 0x0FF0, 0x0000, 0x0000};
	
	short *led_data [] = {&LED_T, &LED_H, &LED_A, &LED_O};
	
	DDRA = 0xff;
	DDRB = 0xff;
	DDRD = 0x00;
	
	EIMSK = 0x03;
	EICRA = 0x0f;
	sei();
	
	while (1){
		for(int led_char_expand_num = 0; led_char_expand_num < data_size ; led_char_expand_num++){
			short *led_char = led_data [led_data_point_num];
			PrintLed(led_char[led_char_expand_num]);
			_delay_ms(6);
		}
	}
}

void PrintLed(short setLed){
	PORTA = setLed >> 8;
	PORTB = setLed;
}